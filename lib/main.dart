import 'package:flutter/material.dart';
import 'package:simple_calculator/src/pages/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(appBarTheme: AppBarTheme(color: Colors.cyan)),
        debugShowCheckedModeBanner: false,
        title: 'Simple Calculator',
        home: HomePage());
  }
}
