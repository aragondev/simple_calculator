import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? a, b;
  int result = 0;
  
  



  @override
  Widget build(BuildContext context) {
    return Scaffold(      
      appBar: AppBar(
        centerTitle: true,
        title: Text('Simple Calculator'),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _inputA(), SizedBox(height: 20,), _inputB(), Divider(), _result(), //_inputB()
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.cyan,
          child: Icon(Icons.add),          
          onPressed: ()async{            
            result = await getSuma();
            setState((){                                                       
            });           
          }),
    );
  }

  Widget _inputA() {
    return TextField(
      keyboardType: TextInputType.number,
      onChanged: (value) {
        setState(() {
          a = value;
        });
      },
      decoration: InputDecoration(          
          labelText: 'Valor A',
          hintText: 'Ingrese un numero: ej: 34',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
          icon: Icon(
            Icons.circle,
            color: Colors.redAccent,
          )),
    );
  }

  Widget _inputB() {
    return TextField(
      keyboardType: TextInputType.number,
      onChanged: (value) {
        setState(() {
          b = value;
        });
      },
      decoration: InputDecoration(
          labelText: 'Valor B',
          hintText: 'Ingrese un numero: ej: 34',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
          icon: Icon(
            Icons.circle,
            color: Colors.amberAccent,
          )),
    );
  }

  Widget _result() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30),
      child: Text(
        'La suma es: $result',
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    );
  }


  

  Future<int> getSuma()async{
    
    final response = await http.get(Uri.parse('https://laravel-api-calculator.herokuapp.com/api/suma/$a/$b'));
    if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.    
    return int.parse(response.body);
    //return int.parse(response.body);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed');
  } 

  }
}
